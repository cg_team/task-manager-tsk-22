package ru.inshakov.tm.api.service;

import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.User;

public interface IUserService extends IService<User> {

    boolean isLoginExists(String login);

    User findByEmail(String email);

    User findByLogin(String login);

    boolean isEmailExists(String email);

    void removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    );

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);
}

