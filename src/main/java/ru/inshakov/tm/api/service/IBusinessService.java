package ru.inshakov.tm.api.service;

import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    E changeStatusById(String userId, String id, Status status);

    E changeStatusByIndex(String userId, Integer index, Status status);

    E changeStatusByName(String userId, String name, Status status);

    void clear(String userId);

    E finishById(String userId, String id);

    E finishByIndex(String userId, Integer index);

    E finishByName(String userId, String name);

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    E findOneById(String userId, String id);

    E findOneByIndex(String userId, Integer index);

    E findOneByName(String userId, String name);

    E removeOneById(String userId, String id);

    E removeOneByIndex(String userId, Integer index);

    E removeOneByName(String userId, String name);

    E startById(String userId, String id);

    E startByIndex(String userId, Integer index);

    E startByName(String userId, String name);

    E updateById(String userId, String id, String name, String description);

    E updateByIndex(String userId, Integer index, String name, String description);


}
