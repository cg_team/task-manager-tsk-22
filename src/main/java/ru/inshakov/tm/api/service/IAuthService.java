package ru.inshakov.tm.api.service;

import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.User;

public interface IAuthService {

    void checkRoles(Role... roles);

    User getUser();

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}

