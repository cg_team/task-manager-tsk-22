package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    void removeByLogin(String login);

}
