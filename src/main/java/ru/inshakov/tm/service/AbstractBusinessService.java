package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.api.service.IBusinessService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.empty.EmptyStatusException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public E changeStatusById(final String userId, final String id, final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeStatusByName(final String userId, final String name, final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public void clear(String userId) {
        entityRepository.clear(userId);
    }

    @Override
    public E finishById(final String userId, String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishByName(final String userId, String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishByIndex(final String userId, Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (!Optional.ofNullable(comparator).isPresent()) return null;
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public List<E> findAll(String userId) {
        return entityRepository.findAll(userId);
    }

    @Override
    public E findOneById(final String userId, String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        return entityRepository.findOneByIndex(userId, index);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeOneById(userId, id);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.removeOneByName(userId, name);
    }

    @Override
    public E startById(final String userId, final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByName(final String userId, String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByIndex(final String userId, final Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity .setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}

