package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.*;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.exception.user.EmailTakenException;
import ru.inshakov.tm.exception.user.LoginTakenException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    public User findByEmail(final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void removeByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    public User create(final String login, final String password, final String email) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isLoginExists(login)) throw new LoginTakenException(login);
        if (isEmailExists(email)) throw new EmailTakenException(email);
        final User user = create(login, password);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        final User user = create(login, password);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;
    }

    public User setPassword (final String userId, final String password) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = userRepository.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = userRepository.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return null;
        user.setLocked(false);
        return user;
    }

}
