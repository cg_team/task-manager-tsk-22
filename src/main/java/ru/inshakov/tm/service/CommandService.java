package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.ICommandRepository;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.api.service.ICommandService;

import java.util.Collection;
import java.util.Optional;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (!Optional.ofNullable(name).isPresent()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public void add(AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        commandRepository.add(command);
    }

}

