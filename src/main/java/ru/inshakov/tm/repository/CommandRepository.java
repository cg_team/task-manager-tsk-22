package ru.inshakov.tm.repository;

import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.api.repository.ICommandRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }

    public AbstractCommand getCommandByArg(String name) {
        return arguments.get(name);
    }

}
