package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> removeAllTasksByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.remove(task);
        }
        return tasksByProjectId;
    }

}
