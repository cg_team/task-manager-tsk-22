package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(String userId) {
        findAll(userId).clear();
    }

    @Override
    public List<E> findAll(String userId) {
        return entities.stream().filter(item -> userId.equals(item.getUserId())).collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entitiesSort = new ArrayList<>(entities);
        entitiesSort.sort(comparator);
        return entitiesSort.stream().filter(item -> userId.equals(item.getUserId())).collect(Collectors.toList());
    }

    @Override
    public E findOneById(final String userId, final String id) {
        return entities.stream().filter(entity -> id.equals(entity.getId()) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        return entities.stream().filter(entity -> entity.equals(entities.get(index)) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        return entities.stream().filter(entity -> name.equals(entity.getName()) && userId.equals(entity.getUserId())).findFirst().orElse(null);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final E entity = findOneById(userId, id);
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final E entity = findOneByIndex(userId, index);
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final E entity = findOneByName(userId, name);
        entities.remove(entity);
        return entity;
    }

}
