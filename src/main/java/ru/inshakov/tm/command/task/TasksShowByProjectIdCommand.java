package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.List;

public class TasksShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Override
    public String description() {
        return "show all tasks of project by project id";
    }

    @Override
    public void execute() {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = IServiceLocator.getProjectTaskService().findAllTasksByProjectId(userId, projectId);
        tasks.forEach(System.out::println);
    }

}
