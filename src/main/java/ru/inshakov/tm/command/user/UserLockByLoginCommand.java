package ru.inshakov.tm.command.user;

import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        IServiceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
