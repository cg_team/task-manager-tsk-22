package ru.inshakov.tm.command.user;

import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String description() {
        return "unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        IServiceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("[OK]");
    }

    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
