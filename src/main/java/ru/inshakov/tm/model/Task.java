package ru.inshakov.tm.model;

import ru.inshakov.tm.api.entity.IWBS;

public class Task extends AbstractBusinessEntity  implements IWBS {

    private String projectId = null;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}